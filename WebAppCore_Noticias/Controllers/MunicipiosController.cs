﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebAppCore_Noticias.Data;
using WebAppCore_Noticias.Models;
using WebAppCore_Noticias.Models.ViewModels;

namespace WebAppCore_Noticias.Controllers
{
    public class MunicipiosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MunicipiosController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Municipio.ToListAsync());
        }

        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var municipio = await _context.Municipio.SingleOrDefaultAsync(m => m.Id == id);

            if (municipio == null)
            {
                return NotFound();
            }

            return View(municipio);
        }

        // GET: Municipios/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Municipios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MunicipioViewModels viewModels)
        {
            if (ModelState.IsValid)
            {
                var model = new Municipio();
                model.Nome = viewModels.Nome;
                model.Uf = viewModels.Uf;

                _context.Add(model);

                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return View(viewModels);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)            
                return NotFound();            

            var municipio = await _context.Municipio.SingleOrDefaultAsync(m => m.Id == id);

            if (municipio == null)            
                return NotFound();            

            return View(municipio);
        }

        // POST: Municipios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, MunicipioViewModels viewModels)
        {
            if (id != viewModels.Id)            
                return NotFound();            

            if (ModelState.IsValid)
            {
                try
                {
                    var model = new Municipio();
                    model.Nome = viewModels.Nome;
                    model.Uf = viewModels.Uf;

                    _context.Update(model);

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MunicipioExists(viewModels.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(viewModels);
        }

        // GET: Municipios/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)            
                return NotFound();            

            var municipio = await _context.Municipio.SingleOrDefaultAsync(m => m.Id == id);

            if (municipio == null)            
                return NotFound();
            
            return View(municipio);
        }

        // POST: Municipios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var municipio = await _context.Municipio.SingleOrDefaultAsync(m => m.Id == id);

            _context.Municipio.Remove(municipio);

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        private bool MunicipioExists(int id)
        {
            return _context.Municipio.Any(e => e.Id == id);
        }
    }
}
