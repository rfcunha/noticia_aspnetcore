﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppCore_Noticias.Models.ViewModels
{
    public class CategoriaViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Descrição")]
        [MaxLength(100)]
        public string Descricao { get; set; }
    }
}
