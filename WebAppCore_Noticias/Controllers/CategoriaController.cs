﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAppCore_Noticias.Data;
using WebAppCore_Noticias.Models;
using WebAppCore_Noticias.Models.ViewModels;

namespace WebAppCore_Noticias.Controllers
{
    [Authorize(Roles = "Categoria")]
    public class CategoriaController : Controller
    {
        private ApplicationDbContext dbContext;
        public CategoriaController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var listaCategorias = dbContext.Categorias.OrderBy(c => c.Descricao);

            return View("Index", listaCategorias);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CategoriaViewModel viewModel)
        {
            if(ModelState.IsValid)
            {
                var model = new Categoria
                {
                    Descricao = viewModel.Descricao
                };

                dbContext.Add(model);
                await dbContext.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            //SE OCORRER ALGUM ERRO FICA NA TELA COM OS DADOS PREENCHIDOS
            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            var objCategoria = await dbContext.Categorias.SingleOrDefaultAsync(x => x.Id == id);

            if (id == null || objCategoria == null)
            {
                return NotFound();
            }

            var viewModel = new CategoriaViewModel();
            viewModel.Id = objCategoria.Id;
            viewModel.Descricao = objCategoria.Descricao;

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CategoriaViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var objCategoria = await dbContext.Categorias.SingleOrDefaultAsync(x => x.Id == viewModel.Id);

                objCategoria.Descricao = viewModel.Descricao;

                dbContext.Update(objCategoria);

                await dbContext.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            //SE OCORRER ALGUM ERRO FICA NA TELA COM OS DADOS PREENCHIDOS
            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            var objCategoria = await dbContext.Categorias.SingleOrDefaultAsync(x => x.Id == id);

            if (id == null || objCategoria == null)
            {
                return NotFound();
            }

            return View(objCategoria);
        }

        [HttpPost]
        //[HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm(int id)
        {
            var objCategoria = dbContext.Categorias.SingleOrDefault(x => x.Id == id);

            dbContext.Remove(objCategoria);

            await dbContext.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}