﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAppCore_Noticias.Data;
using WebAppCore_Noticias.Models;
using WebAppCore_Noticias.Models.ViewModels;

namespace WebAppCore_Noticias.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index(int? filtro = null)
        {
            var viewModel = new HomeViewModels();
            viewModel.Categorias = _context.Categorias.OrderBy(x => x.Descricao);
            viewModel.Banner = _context.Noticias.Where(x => x.Destaque).OrderByDescending(x => x.DataPublicacao).Take(5);

            if (filtro == null)
                viewModel.UltimasNoticias = _context.Noticias.OrderByDescending(x => x.DataPublicacao).Take(3);
            else
                viewModel.UltimasNoticias = _context.Noticias.Where(x => x.CategoriaId == filtro).OrderByDescending(x => x.DataPublicacao).Take(3);


            return View(viewModel);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
