﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppCore_Noticias.Models
{
    public class Categoria:Base
    {
        //[Required]
        [Display(Name = "Descrição")]
        [MaxLength(100)]
        public string Descricao { get; set; }

        public ICollection<Noticia> Noticias { get; set; }
    }
}
