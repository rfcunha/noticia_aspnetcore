﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppCore_Noticias.Models.ViewModels
{
    public class MunicipioViewModels
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Município")]
        [MaxLength(100)]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "Estado")]
        [MaxLength(2)]
        public string Uf { get; set; }
    }
}
