﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppCore_Noticias.Models
{
    public abstract class Base
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string UsuarioCriacao { get; set; }
        [MaxLength(100)]
        public string UsuarioAlteracao { get; set; }
        public DateTime? DataCriacao { get; set; }
        public DateTime? DataAlteracao { get; set; }
    }
}
