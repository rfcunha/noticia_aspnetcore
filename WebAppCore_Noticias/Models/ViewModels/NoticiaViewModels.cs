﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppCore_Noticias.Models.ViewModels
{
    public class NoticiaViewModels
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "digite o titulo")]
        [Display(Name = "Título")]
        [MaxLength(256)]
        public string Titulo { get; set; }
        public string Corpo { get; set; }
        [Required(ErrorMessage = "digite a data de publicação")]
        [Display(Name = "Data da Publicação")]
        public DateTime? DataPublicacao { get; set; }
        [Required(ErrorMessage = "digite o autor")]
        [MaxLength(200)]
        public string Autor { get; set; }
        public bool Destaque { get; set; }
        [Required(ErrorMessage = "selecione a categoria")]
        [Display(Name = "Categoria")]
        public int? CategoriaId { get; set; }
        public SelectList ListaCategorias { get; set; }
    }
}
