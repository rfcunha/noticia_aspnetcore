﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebAppCore_Noticias.Data;
using WebAppCore_Noticias.Models;
using WebAppCore_Noticias.Models.ViewModels;

namespace WebAppCore_Noticias.Controllers
{
    [Authorize(Roles = "Noticia")]
    public class NoticiasController : Controller
    {
        private ApplicationDbContext dbContext;
        public NoticiasController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var listaNoticias = dbContext.Noticias.OrderByDescending(c => c.DataPublicacao);

            return View("Index", listaNoticias);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var viewModel = new NoticiaViewModels();
            viewModel.ListaCategorias = new SelectList(dbContext.Categorias.OrderBy(x => x.Descricao), "Id", "Descricao");
            viewModel.DataPublicacao = DateTime.Parse(DateTime.Now.ToString("d"), CultureInfo.CreateSpecificCulture("pt-BR"));

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(NoticiaViewModels viewModel)
        {
            if(ModelState.IsValid)
            {
                var model = new Noticia
                {
                    Titulo = viewModel.Titulo,
                    Autor = viewModel.Autor,
                    Corpo = viewModel.Corpo,
                    DataPublicacao = viewModel.DataPublicacao,
                    Destaque = viewModel.Destaque,
                    CategoriaId = viewModel.CategoriaId
                };

                dbContext.Add(model);
                await dbContext.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            //SE OCORRER ALGUM ERRO FICA NA TELA COM OS DADOS PREENCHIDOS
            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            var objNoticias = await dbContext.Noticias.SingleOrDefaultAsync(x => x.Id == id);

            if (id == null || objNoticias == null)
            {
                return NotFound();
            }

            var viewModel = new NoticiaViewModels();

            viewModel.ListaCategorias = new SelectList(dbContext.Categorias.OrderBy(x => x.Descricao), "Id", "Descricao");            

            viewModel.Id = objNoticias.Id;
            viewModel.Titulo = objNoticias.Titulo;
            viewModel.Autor = objNoticias.Autor;
            viewModel.Corpo = objNoticias.Corpo;
            viewModel.DataPublicacao = objNoticias.DataPublicacao;
            viewModel.Destaque = objNoticias.Destaque;
            viewModel.CategoriaId = objNoticias.CategoriaId;

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(NoticiaViewModels viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = await dbContext.Noticias.SingleOrDefaultAsync(x => x.Id == viewModel.Id);

                model.Titulo = viewModel.Titulo;
                model.Autor = viewModel.Autor;
                model.Corpo = viewModel.Corpo;
                model.DataPublicacao = viewModel.DataPublicacao;
                model.Destaque = viewModel.Destaque;
                model.CategoriaId = viewModel.CategoriaId;

                dbContext.Update(model);

                await dbContext.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            //SE OCORRER ALGUM ERRO FICA NA TELA COM OS DADOS PREENCHIDOS
            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            var objNoticias = await dbContext.Noticias.SingleOrDefaultAsync(x => x.Id == id);

            if (id == null || objNoticias == null)
            {
                return NotFound();
            }

            return View(objNoticias);
        }

        [HttpPost]
        //[HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirm(int id)
        {
            var objNoticias = dbContext.Noticias.SingleOrDefault(x => x.Id == id);

            dbContext.Remove(objNoticias);

            await dbContext.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}